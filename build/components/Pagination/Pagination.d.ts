import React from 'react';
export interface PaginationProps {
    totalItems: number;
    itemsPerPage: number;
    maxPageNumbersDisplayed: number;
    onPaginate: Function;
    currentPage?: number;
    vSize?: number;
    variant?: 'bordered';
    mainColor?: string;
    textColor?: string;
    backgroundColor?: string;
    hoverColor?: string;
    borderColor?: string;
    className?: string;
    disableArrows?: boolean;
    enableStrokeAnimation?: boolean;
    enableArrowBorder?: boolean;
    enableArrowBackground?: boolean;
    enableArrowCheck?: boolean;
}
export declare const Pagination: React.ForwardRefExoticComponent<PaginationProps & React.RefAttributes<unknown>>;
